package com.mnombewu.trendyapp

import android.app.Application
import timber.log.Timber
import timber.log.Timber.DebugTree


/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
class TrendyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(DebugTree())
    }
}
package com.mnombewu.trendyapp.ui.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mnombewu.common.Constants
import com.mnombewu.trendyapp.State
import com.mnombewu.tweetcontent.di.ContentInjector
import com.mnombewu.tweetcontent.model.Tweet
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class HomeViewModel(application: Application) : AndroidViewModel(application) {
    private var disposable: Disposable? = null
    private val _tweets = MutableLiveData<State<List<Tweet>>>()
    val tweets: LiveData<State<List<Tweet>>> = _tweets

    fun getAfricanacityTweets() {
        _tweets.value = State.loading(null)
        val tweetRepository = ContentInjector.provideTweetRepository(getApplication())
        disposable = tweetRepository.getTweets(Constants.ABSA_TERM, Constants.REQUEST_ORDER_MIXED)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ tweets ->
                Timber.v("Tweets: %s", tweets)
                _tweets.value = State.success(tweets)
            }, { error ->
                Timber.e(error)
                _tweets.value = State.error(error, null)
            })
    }

    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.dispose()
            disposable = null
        }
    }
}
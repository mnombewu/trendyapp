package com.mnombewu.trendyapp.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mnombewu.trendyapp.R
import com.mnombewu.trendyapp.State
import com.mnombewu.trendyapp.utils.TweetAdapter
import com.mnombewu.trendyapp.utils.longToast
import kotlinx.android.synthetic.main.circular_progress_bar.*
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        homeViewModel.tweets.observe(viewLifecycleOwner, Observer {
            if (it.status == State.Status.LOADING) {
                showProgress(true)
                return@Observer
            }

            showProgress(false)
            if (it.status == State.Status.ERROR) {
                it.throwable?.message?.let { errorMessage -> longToast(errorMessage) }
                return@Observer
            }

            val tweets = it.data
            if (tweets.isNullOrEmpty()) {
                no_tweets_home.text = getString(R.string.no_tweets)
                tweet_list_home.visibility = View.INVISIBLE
            } else {
                no_tweets_home.visibility = View.INVISIBLE
                val layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                val dividerItemDecoration = DividerItemDecoration(
                    tweet_list_home.context,
                    layoutManager.orientation
                )
                val adapter = TweetAdapter()
                tweet_list_home.layoutManager = layoutManager
                tweet_list_home.addItemDecoration(dividerItemDecoration)
                tweet_list_home.adapter = adapter
                adapter.setTweets(tweets)
            }

        })

        homeViewModel.getAfricanacityTweets()
    }

    private fun showProgress(isShow: Boolean) {
        if (isShow) {
            circular_progress_bar.visibility = View.VISIBLE
        } else {
            circular_progress_bar.visibility = View.GONE
        }
    }
}
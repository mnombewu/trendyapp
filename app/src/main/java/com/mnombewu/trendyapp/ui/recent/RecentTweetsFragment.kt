package com.mnombewu.trendyapp.ui.recent

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mnombewu.trendyapp.R
import com.mnombewu.trendyapp.State
import com.mnombewu.trendyapp.utils.TweetAdapter
import com.mnombewu.trendyapp.utils.longToast
import kotlinx.android.synthetic.main.circular_progress_bar.*
import kotlinx.android.synthetic.main.fragment_recent.*

//TODO: HomeFragment and RecentTweeetsFragment are quite similar, can be replaced into single fragment
class RecentTweetsFragment : Fragment() {

    private lateinit var recentTweetsViewModel: RecentTweetsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        recentTweetsViewModel =
            ViewModelProvider(this).get(RecentTweetsViewModel::class.java)
        return inflater.inflate(R.layout.fragment_recent, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recentTweetsViewModel.tweets.observe(viewLifecycleOwner, Observer {
            if (it.status == State.Status.LOADING) {
                showProgress(true)
                return@Observer
            }

            showProgress(false)
            if (it.status == State.Status.ERROR) {
                it.throwable?.message?.let { errorMessage -> longToast(errorMessage) }
                return@Observer
            }

            val tweets = it.data
            if (tweets.isNullOrEmpty()) {
                text_recent.text = getString(R.string.no_tweets)
                tweet_list_absa.visibility = View.INVISIBLE
            } else {
                text_recent.visibility = View.INVISIBLE
                val layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                val dividerItemDecoration = DividerItemDecoration(
                    tweet_list_absa.context,
                    layoutManager.orientation
                )
                val adapter = TweetAdapter()
                tweet_list_absa.layoutManager = layoutManager
                tweet_list_absa.adapter = adapter
                tweet_list_absa.addItemDecoration(dividerItemDecoration)
                adapter.setTweets(tweets)
            }
        })

        recentTweetsViewModel.getTaggedTweets()
    }

    private fun showProgress(isShow: Boolean) {
        if (isShow) {
            circular_progress_bar.visibility = View.VISIBLE
        } else {
            circular_progress_bar.visibility = View.GONE
        }
    }
}
package com.mnombewu.trendyapp.utils

import android.widget.Toast
import androidx.fragment.app.Fragment

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */

fun Fragment.longToast(message: String) {
    Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
}
package com.mnombewu.trendyapp.utils

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mnombewu.trendyapp.R
import com.mnombewu.tweetcontent.model.Tweet

/**
 * @author mongezi
 * @since 2020-03-06
 *
 * m.r.nombewu@gmail.com
 */
class TweetViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private var tweet: Tweet? = null
    private var userTextView: TextView = itemView.findViewById(R.id.tweet_user_title)
    private var messageTextView: TextView = itemView.findViewById(R.id.tweet_text)
    private var userImageView: ImageView = itemView.findViewById(R.id.tweet_user_image)

    fun bind(newTweet: Tweet) {
        tweet = newTweet
        userTextView.text = newTweet.user.name
        messageTextView.text = newTweet.message
        Glide.with(itemView)
            .load(newTweet.user.profileImage)
            .circleCrop()
            .into(userImageView)
    }
}
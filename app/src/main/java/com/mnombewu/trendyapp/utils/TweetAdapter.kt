package com.mnombewu.trendyapp.utils

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mnombewu.trendyapp.R
import com.mnombewu.tweetcontent.model.Tweet

/**
 * @author mongezi
 * @since 2020-03-06
 *
 * m.r.nombewu@gmail.com
 */
class TweetAdapter : RecyclerView.Adapter<TweetViewHolder>() {
    private var tweets = listOf<Tweet>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TweetViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TweetViewHolder(inflater.inflate(R.layout.tweet_item, parent, false))
    }

    override fun getItemCount(): Int {
        return tweets.size
    }

    override fun getItemId(position: Int): Long {
        return tweets.get(position).id
    }

    override fun onBindViewHolder(holder: TweetViewHolder, position: Int) {
        val tweet = tweets.get(position)
        holder.bind(tweet)
    }

    fun setTweets(tweetList: List<Tweet>) {
        tweets = tweetList
        //No need to use ListAdapter since data only changes once
        notifyDataSetChanged()
    }
}
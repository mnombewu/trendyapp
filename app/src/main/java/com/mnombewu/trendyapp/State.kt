package com.mnombewu.trendyapp

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
class State<T> private constructor(val status: Status, val data: T?, val throwable: Throwable?) {

    enum class Status {
        SUCCESS, ERROR, LOADING
    }

    companion object {

        fun <T> success(data: T): State<T> {
            return State(Status.SUCCESS, data, null)
        }

        fun <T> error(exception: Throwable?, data: T?): State<T> {
            return State(Status.ERROR, data, exception)
        }

        fun <T> loading(data: T?): State<T> {
            return State(Status.LOADING, data, null)
        }
    }
}
package com.mnombewu.trendyapp

import org.junit.Test

import org.junit.Assert.*

/**
 * Unit Tests for ViewModels
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ViewModelUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}

package com.mnombewu.authentication.di

import android.content.Context
import com.mnombewu.authentication.repository.AuthenticationRepository
import com.mnombewu.authentication.repository.TwitterAuthenticationRepository
import com.mnombewu.authentication.repository.remote.service.TokenService
import com.mnombewu.common.di.Injector
import com.mnombewu.storage.di.StorageInjector

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
object AuthInjector {
    private var twitterAuthenticationRepository: TwitterAuthenticationRepository? = null

    internal fun provideTokenService(): TokenService {
        return Injector.provideApiService(TokenService::class.java)
    }

    fun provideTwitterAuthRepository(context: Context): AuthenticationRepository {
        return twitterAuthenticationRepository ?: synchronized(this) {
            TwitterAuthenticationRepository(StorageInjector.provideStorage(context.applicationContext)).also {
                twitterAuthenticationRepository = it
            }
        }
    }
}
package com.mnombewu.authentication.error

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
class CredentialsInvalidEception(message: String) : Exception(message)
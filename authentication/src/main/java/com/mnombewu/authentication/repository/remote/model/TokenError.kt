package com.mnombewu.authentication.repository.remote.model

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
data class TokenError(val message: String, val code: Int, val label: String?)
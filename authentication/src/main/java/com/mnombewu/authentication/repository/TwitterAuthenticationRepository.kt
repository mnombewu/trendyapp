package com.mnombewu.authentication.repository

import android.net.Uri
import android.util.Base64
import android.util.Base64.NO_WRAP
import com.mnombewu.authentication.di.AuthInjector
import com.mnombewu.authentication.error.CredentialsInvalidEception
import com.mnombewu.authentication.repository.remote.model.TokenResponseDto
import com.mnombewu.common.Constants
import com.mnombewu.storage.repository.StorageRepository
import io.reactivex.Single
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import okhttp3.FormBody
import retrofit2.Response
import timber.log.Timber

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
class TwitterAuthenticationRepository(private val storage: StorageRepository) :
    AuthenticationRepository {
    companion object {
        const val HEADER_PREFIX = "Basic"
        const val STORAGE_TOKEN_KEY = "access_token"
        const val RESPONSE_UNEXPECTED_MESSAGE = "Unexpected Response"
        const val REQUEST_GRANT_TYPE_VALUE = "client_credentials"
        const val REQUEST_GRANT_TYPE_KEY = "grant_type"
    }

    override fun getToken(): Single<String> {
        //TODO: get stored token first, then get new token if expired
        val api = AuthInjector.provideTokenService()
        val requestDto = FormBody.Builder()
            .addEncoded(REQUEST_GRANT_TYPE_KEY, REQUEST_GRANT_TYPE_VALUE)
            .build()
        val base64Auth = getBase64Auth()
        val auth = "$HEADER_PREFIX $base64Auth"

        return api.getToken(auth, requestDto)
            .subscribeOn(Schedulers.io())
            .map(object : Function<Response<TokenResponseDto>, String> {
                override fun apply(response: Response<TokenResponseDto>): String {
                    val responseDto = response.body()
                        ?: throw CredentialsInvalidEception(RESPONSE_UNEXPECTED_MESSAGE)

                    if (response.isSuccessful) {
                        //probably should validate the body, but keeping it simple
                        val accessToken = responseDto.token ?: ""
                        storage.putString(STORAGE_TOKEN_KEY, accessToken)
                        Timber.v("Saved Access Token - %s", accessToken)

                        return accessToken
                    } else {
                        if (responseDto.errors.isNullOrEmpty()) {
                            throw CredentialsInvalidEception(RESPONSE_UNEXPECTED_MESSAGE)
                        } else {
                            throw CredentialsInvalidEception(responseDto.errors.first().message)
                        }
                    }
                }
            })
    }

    private fun getBase64Auth(): String {
        val encodedKey = Uri.encode(Constants.CONSUMER_KEY)
        val encodedSecret = Uri.encode(Constants.CONSUMER_SECRET)
        val combinedkeys = "$encodedKey:$encodedSecret"
        return Base64.encodeToString(combinedkeys.toByteArray(), NO_WRAP)
    }
}
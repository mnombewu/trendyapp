package com.mnombewu.authentication.repository.remote.model

import com.google.gson.annotations.SerializedName

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
data class TokenResponseDto(
    @SerializedName("tokenType") val type: String?, @SerializedName("access_token") val token: String?,
    val errors: List<TokenError>?
)
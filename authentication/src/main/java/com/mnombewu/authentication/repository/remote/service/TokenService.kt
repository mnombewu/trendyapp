package com.mnombewu.authentication.repository.remote.service

import com.mnombewu.authentication.repository.remote.model.TokenResponseDto
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
interface TokenService {
    @Headers("Content-Type: application/x-www-form-urlencoded;charset=UTF-8")
    @POST("/oauth2/token")
    fun getToken(@Header("Authorization") authorization: String, @Body requestDto: RequestBody): Single<Response<TokenResponseDto>>
}
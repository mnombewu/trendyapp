package com.mnombewu.authentication.repository.remote.model

import com.google.gson.annotations.SerializedName

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
data class TokenRequestDto(@SerializedName("grant_type") val grantType: String = "client_credentials")
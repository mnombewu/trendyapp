package com.mnombewu.authentication.repository

import io.reactivex.Single

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
interface AuthenticationRepository {
    fun getToken(): Single<String>
}
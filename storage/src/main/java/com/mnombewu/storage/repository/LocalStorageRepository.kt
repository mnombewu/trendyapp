package com.mnombewu.storage.repository

import android.content.Context
import android.content.SharedPreferences

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
class LocalStorageRepository(context: Context) : StorageRepository {
    companion object {
        const val SHARED_PREF_NAME = "com.mnombewu.storage.local"
    }

    private val sharedPrefs: SharedPreferences by lazy {
        context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
    }

    override fun putString(key: String, value: String) {
        with(sharedPrefs.edit()) {
            putString(key, value)
            apply()
        }
    }

    override fun getString(key: String): String? {
        return sharedPrefs.getString(key, null)
    }
}
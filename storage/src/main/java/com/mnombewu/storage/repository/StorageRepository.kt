package com.mnombewu.storage.repository

import androidx.annotation.WorkerThread

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
@WorkerThread
interface StorageRepository {
    fun putString(key: String, value: String)

    fun getString(key: String): String?
}
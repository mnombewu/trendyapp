package com.mnombewu.storage.di

import android.content.Context
import com.mnombewu.storage.repository.LocalStorageRepository
import com.mnombewu.storage.repository.StorageRepository

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
object StorageInjector {
    fun provideStorage(context: Context): StorageRepository {
        return LocalStorageRepository(context)
    }
}
package com.mnombewu.tweetcontent.util

import com.mnombewu.tweetcontent.model.Tweet
import com.mnombewu.tweetcontent.model.User
import com.mnombewu.tweetcontent.repository.remote.model.TweetDto
import com.mnombewu.tweetcontent.repository.remote.model.UserDto
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
internal object ObjectMapper {
    const val dateTimeFormat = "EEE MMM dd HH:mm:ss Z yyyy"
    private fun toTweet(tweetDto: TweetDto): Tweet {
        return Tweet(
            tweetDto.id,
            toZonedDateTime(tweetDto.dateCreated),
            tweetDto.text,
            tweetDto.retweetCount,
            tweetDto.favouriteCount,
            toUser(tweetDto.userDto)
        )
    }

    fun toTweets(tweetDtos: List<TweetDto>): List<Tweet> {
        val tweets = mutableListOf<Tweet>()
        tweetDtos.forEach {
            tweets.add(toTweet(it))
        }

        return tweets.toList()
    }

    fun toUser(userDto: UserDto) =
        User(userDto.id, userDto.name, userDto.description, userDto.profileImage)

    private fun toZonedDateTime(date: String): ZonedDateTime {
        val formatter = DateTimeFormatter.ofPattern(dateTimeFormat)
        return ZonedDateTime.parse(date, formatter)
    }

}
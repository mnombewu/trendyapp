package com.mnombewu.tweetcontent.repository.remote.service

import com.mnombewu.tweetcontent.repository.remote.model.TweetSearchResponseDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
internal interface TweetSearchApiService {

    @GET("/{version}/search/tweets.json")
    fun getNearbyPopularTweets(
        @Header("Authorization") authorization: String,
        @Path("version") version: String,
        @Query("q") q: String,
        @Query("count") count: Int,
        @Query("result_type") order: String,
        @Query("geocode", encoded = true) geocode: String
    ): Single<TweetSearchResponseDto>

    @GET("/{version}/search/tweets.json")
    fun getTweets(
        @Header("Authorization") authorization: String,
        @Path("version") version: String,
        @Query("q") q: String,
        @Query("count") count: Int,
        @Query("result_type") order: String
    ): Single<TweetSearchResponseDto>
}
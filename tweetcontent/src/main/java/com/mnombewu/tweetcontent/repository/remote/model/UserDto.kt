package com.mnombewu.tweetcontent.repository.remote.model

import com.google.gson.annotations.SerializedName

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
internal data class UserDto(
    val id: Long,
    val name: String,
    val description: String, @SerializedName("profile_image_url_https") val profileImage: String
)
package com.mnombewu.tweetcontent.repository.remote.model

import com.google.gson.annotations.SerializedName

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
internal data class TweetDto(
    val id: Long,
    val text: String,
    @SerializedName("user") val userDto: UserDto,
    @SerializedName("created_at") val dateCreated: String,
    @SerializedName("retweet_count") val retweetCount: Long,
    @SerializedName("favorite_count") val favouriteCount: Long
)
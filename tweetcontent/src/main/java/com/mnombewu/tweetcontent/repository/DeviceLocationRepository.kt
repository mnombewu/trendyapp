package com.mnombewu.tweetcontent.repository

import android.content.Context
import android.location.Criteria
import android.location.LocationManager

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
class DeviceLocationRepository(private val context: Context) : LocationRepository {
    override fun getLocation(): Pair<Double, Double>? {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val criteria = Criteria().apply {
            accuracy = Criteria.ACCURACY_COARSE
        }
        val provider = locationManager.getBestProvider(criteria, false)
        val location = locationManager.getLastKnownLocation(provider)

        return if (location == null) {
            null
        } else {
            return Pair(location.longitude, location.latitude)
        }
    }
}
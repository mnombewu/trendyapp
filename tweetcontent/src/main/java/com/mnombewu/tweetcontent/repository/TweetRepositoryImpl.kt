package com.mnombewu.tweetcontent.repository

import com.mnombewu.authentication.repository.AuthenticationRepository
import com.mnombewu.common.Constants
import com.mnombewu.common.di.Injector
import com.mnombewu.tweetcontent.model.Tweet
import com.mnombewu.tweetcontent.repository.remote.service.TweetSearchApiService
import com.mnombewu.tweetcontent.util.ObjectMapper
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
class TweetRepositoryImpl(
    private val authRepo: AuthenticationRepository
) : TweetRepository {
    companion object {
        const val REQUEST_TWEET_COUNT = 100
        const val REQUEST_AUTHORIZATION_PREFIX = "Bearer"
    }

    override fun getNearbyPopularTweetsNearMe(): Single<List<Tweet>> {
        TODO("not implemented")
    }

    override fun getTweets(searchTerm: String, order: String): Single<List<Tweet>> {
        return authRepo.getToken()
            .subscribeOn(Schedulers.io())
            .flatMap { token ->
                val api = Injector.provideApiService(TweetSearchApiService::class.java)
                val bearerToken = "$REQUEST_AUTHORIZATION_PREFIX ${token}"
                api.getTweets(
                    bearerToken,
                    Constants.API_VERSION,
                    searchTerm,
                    REQUEST_TWEET_COUNT,
                    order
                )
            }
            .map { responseDto -> ObjectMapper.toTweets(responseDto.tweets) }
    }
}
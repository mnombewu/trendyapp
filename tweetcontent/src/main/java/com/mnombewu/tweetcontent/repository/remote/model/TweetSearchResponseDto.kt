package com.mnombewu.tweetcontent.repository.remote.model

import com.google.gson.annotations.SerializedName

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
internal data class TweetSearchResponseDto(
    @SerializedName("statuses") val tweets: List<TweetDto>
    //TODO add metadata for paging
)
package com.mnombewu.tweetcontent.repository

import com.mnombewu.tweetcontent.model.Tweet
import io.reactivex.Single

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
interface TweetRepository {
    fun getNearbyPopularTweetsNearMe(): Single<List<Tweet>>

    fun getTweets(searchTerm: String, order: String): Single<List<Tweet>>
}
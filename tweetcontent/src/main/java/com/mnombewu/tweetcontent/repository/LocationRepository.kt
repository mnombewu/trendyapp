package com.mnombewu.tweetcontent.repository

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * Location repository to keep Android OS dependencies contained
 *
 * m.r.nombewu@gmail.com
 */
interface LocationRepository {
    /**
     * Retrieve location co-ordinates (Longitude, Latitude)
     *
     * @return Pair<String, String> Returns a Pair containing longitude (first) and latitude (second)
     */
    fun getLocation(): Pair<Double, Double>?
}
package com.mnombewu.tweetcontent.model

import org.threeten.bp.ZonedDateTime

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
//TODO: add retreeted tweet
class Tweet(
    val id: Long,
    val dateCreated: ZonedDateTime,
    val message: String,
    val retreetCount: Long,
    val favouriteCount: Long,
    val user: User
)
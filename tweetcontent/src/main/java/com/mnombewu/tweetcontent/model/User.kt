package com.mnombewu.tweetcontent.model

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
data class User(val id: Long, val name: String, val description: String, val profileImage: String)
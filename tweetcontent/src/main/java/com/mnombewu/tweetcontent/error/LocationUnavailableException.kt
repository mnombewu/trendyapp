package com.mnombewu.tweetcontent.error

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
class LocationUnavailableException(message: String) : Exception(message) {
}
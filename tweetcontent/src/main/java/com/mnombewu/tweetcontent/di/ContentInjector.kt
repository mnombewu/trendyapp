package com.mnombewu.tweetcontent.di

import android.content.Context
import com.mnombewu.authentication.di.AuthInjector
import com.mnombewu.tweetcontent.repository.DeviceLocationRepository
import com.mnombewu.tweetcontent.repository.LocationRepository
import com.mnombewu.tweetcontent.repository.TweetRepository
import com.mnombewu.tweetcontent.repository.TweetRepositoryImpl

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
object ContentInjector {
    private var tweetRepository: TweetRepository? = null
    private var locationRepository: LocationRepository? = null

    fun provideTweetRepository(context: Context): TweetRepository {
        return tweetRepository ?: synchronized(this) {
            TweetRepositoryImpl(
                AuthInjector.provideTwitterAuthRepository(context)
            )
        }
    }

    fun provideLocationRepository(context: Context): LocationRepository {
        return locationRepository ?: synchronized(this) {
            DeviceLocationRepository(context.applicationContext).also {
                locationRepository = it
            }
        }
    }
}
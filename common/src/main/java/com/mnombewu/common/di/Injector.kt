package com.mnombewu.common.di

import com.mnombewu.common.BuildConfig
import com.mnombewu.common.Constants
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 * @author mongezi
 * @since 2020-03-05
 *
 * m.r.nombewu@gmail.com
 */
object Injector {
    private var retroFitInstance: Retrofit? = null

    fun provideRetrofit(): Retrofit {
        return retroFitInstance ?: synchronized(this) {
            val okHttpClient = getOkHttpClinet()
            val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())


            retrofit.build().also { retroFitInstance = it }
        }
    }

    fun <T> provideApiService(service: Class<T>): T {
        return provideRetrofit().create(service)
    }


    private fun getOkHttpClinet(): OkHttpClient {
        val logLevel = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }

        return OkHttpClient.Builder()
            .addInterceptor(getTwitterSearchInterceptor())
            .addInterceptor(HttpLoggingInterceptor().setLevel(logLevel))
            .connectTimeout(Constants.NETWORK_TIMEOUT_SECONDS, TimeUnit.SECONDS).build()
    }

    private fun getTwitterSearchInterceptor(): Interceptor {
        return object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val originalUrl = chain.request().url()
                val query = originalUrl.encodedQuery()
                return if (query == null) {
                    chain.proceed(chain.request())
                } else {
                    val builder: Request.Builder = chain.request().newBuilder()
                    builder.url(
                        originalUrl.newBuilder()
                            .encodedQuery(query.replace("q=&", "q="))
                            .build()
                    )
                    chain.proceed(builder.build())
                }
            }

        }
    }
}
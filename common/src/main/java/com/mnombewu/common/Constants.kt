package com.mnombewu.common

/**
 * @author mongezi
 * @since 2020-03-05
 *
 * Config Class
 *
 * m.r.nombewu@gmail.com
 */
object Constants {
    const val BASE_URL = "https://api.twitter.com"
    const val API_VERSION = "1.1"
    const val BASE_API_URL = "$BASE_URL/$API_VERSION"
    const val NETWORK_TIMEOUT_SECONDS = 10L
    const val CONSUMER_KEY = "7nop2Y2JJ6vJz6dNr84lCNIpc"
    const val CONSUMER_SECRET = "eM6Lb1dZeoB0Y2jhdzSFIzlm07kdvxPaUCxby57AlTnO0oL02j"
    const val ABSA_TWITTER_USER = "AbsaSouthAfrica"
    const val ABSA_TERM = "africanacity"
    const val REQUEST_ORDER_POPULAR = "popular"
    const val REQUEST_ORDER_RECENT = "recent"
    const val REQUEST_ORDER_MIXED = "mixed"
}